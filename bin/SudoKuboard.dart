import 'Number.dart';

class SudoKuboard{
    var board =[[2,5,0,0,6,0],
                [0,0,0,0,5,4],
                [0,0,0,4,0,0],
                [0,4,6,0,0,0],
                [6,0,5,0,0,1],
                [0,1,0,0,0,5]];
    int? _row;
    int? _col;
    String? menu;
    Number? num1;
    Number? num2;
    Number? num3;
    Number? num4;
    Number? num5;
    Number? num6;

    SudoKuboard(Number num1 , Number num2, Number num3, Number num4, Number num5, Number num6){
      this.num1=num1;
      this.num2=num2;
      this.num3=num3;
      this.num4=num4;
      this.num5=num5;
      this.num6=num6;
      
    }

}