class Number {
  int? number;
  
  Number(int number) {
    this.number = number;
  }

  get getNumber {
    return number;
  }

  set setNumber(int number) {
    this.number = number;
  }

}
